/* IMPORTANT: 

If you're on macOS, you can skip this instruction. 
On Windows, you may need to set the environment variable OPENSSL_CONF to the absolute path to your openssl.cnf or openssl.cfg file within the directory where you installed OpenSSL. 
The file's name and location may vary from the following instructions depending on which OpenSSL program you installed. The following command is an example. 
If your OpenSSL installation directory or one of its sub-directories doesn't have an openssl.cnf file, sometimes the configuration file is named openssl.cfg.

> set OPENSSL_CONF=C:\PathToOpenSSL\openssl.cnf

*/

const execSync = require('child_process').execSync;
const arg = process.argv[2];

const fs = require('fs');
let cert = './certificates';
let assets = './assets';

process.chdir("../../..");
if (!fs.existsSync(cert)) {
    fs.mkdirSync(cert);
    process.chdir(cert);
} else {
    process.chdir(cert);
}

/* From within the certificates folder, generate an RSA private key. For the pass: argument, you can choose a different password other than SomePassword. */
execSync('openssl genrsa -des3 -passout pass:' + arg + ' -out server.pass.key 4096');

/* Create a key file from the server.pass.key file. For the pass: argument, use the same password from the prior command. */
execSync('openssl rsa -passin pass:' + arg + ' -in server.pass.key -out server.key');

/* Delete the server.pass.key file. It's no longer needed. */
fs.unlinkSync('server.pass.key');

/* Generate a certificate signing request using the server.key file. Store the certificate signing request in a file called server.csr. Enter information about your company when prompted. */
execSync('openssl req -new -key server.key -subj "/C=DE/ST=HH/L=Hamburg/O=hanseflow/CN=www.hanseflow.de" -out server.csr');

/* Generate the SSL certificate. It's generated from the server.key private key and server.csr files. */
execSync('openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt');

/* Encrypt the server.key file so that you can safely add it to your Git repository in the next step. For the -k argument, you can choose a different password other than SomePassword. */
execSync('openssl enc -aes-256-cbc -md sha256 -salt -e -in server.key -out server.key.enc -k ' + arg + ' -pbkdf2');
